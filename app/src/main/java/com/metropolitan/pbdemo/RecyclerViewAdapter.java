package com.metropolitan.pbdemo;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

/**
 * Created by igor on 5/22/18.
 */

public class RecyclerViewAdapter extends RecyclerView.Adapter<RecyclerViewAdapter.MyViewHolder> {

    private Context mContext;
    private List<Dot> mData;


    public RecyclerViewAdapter(Context mContext, List<Dot> mData) {
        this.mContext = mContext;
        this.mData = mData;
    }

    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view;

        LayoutInflater mInflater = LayoutInflater.from(mContext);
        view = mInflater.inflate(R.layout.card_item_dot, parent, false);


        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, final int position) {

        holder.tv_dot_title.setText(mData.get(position).getTitle());
        holder.img_dot_thumbnail.setImageResource(mData.get(position).getThumbnail());
        holder.cardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(mContext, Dot_Activity.class);
                // passing data to the dots activity
                intent.putExtra("Title", mData.get(position).getTitle());
                intent.putExtra("Description", mData.get(position).getDescription());
                intent.putExtra("Category", mData.get(position).getCategory());
                intent.putExtra("Thumbnail", mData.get(position).getThumbnail());
                //start the activity
                mContext.startActivity(intent);


            }
        });
        // Set click listener


    }

    @Override
    public int getItemCount() {
        return mData.size();
    }

    public static class MyViewHolder extends RecyclerView.ViewHolder {

        TextView tv_dot_title;
        ImageView img_dot_thumbnail;
        CardView cardView;

        public MyViewHolder(View itemView) {
            super(itemView);

            tv_dot_title = (TextView) itemView.findViewById(R.id.dot_title_id);
            img_dot_thumbnail = (ImageView) itemView.findViewById(R.id.dot_img_id);
            cardView = (CardView) itemView.findViewById(R.id.cardview_id);

        }

    }


}
