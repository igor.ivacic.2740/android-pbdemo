package com.metropolitan.pbdemo;

import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.TextView;

/**
 * Created by igor on 5/28/18.
 */

public class TeamMemberActivity extends MainActivity{
    private TextView tv_title, tv_description, tv_cat;
    private ImageView imageView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_teammember);


        tv_title = (TextView) findViewById(R.id.title1);
        tv_description = (TextView) findViewById(R.id.description1);
        tv_cat = (TextView) findViewById(R.id.cat_id1);
        imageView = (ImageView) findViewById(R.id.img_id1);

        //Recieve data
        Intent intent = getIntent();
        String Title = intent.getExtras().getString("FirstName");
        String Category = intent.getExtras().getString("LastName");
        String Description = intent.getExtras().getString("Position");
        int image = intent.getExtras().getInt("Thumbnail");

        //Setting values

        tv_title.setText(Title);
        tv_description.setText(Description);
        tv_cat.setText(Category);
        imageView.setImageResource(image);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case R.id.refresh_page:
                super.hideAndReconect();
                return true;

            case R.id.exit_app:
                super.onExit();
                return true;
            case R.id.make_a_call:
                super.makeACall();
                return true;
            case R.id.technologies:
                super.openTechnologies();
                return true;

            case R.id.zaposleni:
                teamMethod();
                return true;
            case R.id.see_gallery:
                super.showGallery();
                return true;

            case R.id.about_us:
                super.aboutUs();
                return true;
            case R.id.send_email:
                super.showEmail();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

}
