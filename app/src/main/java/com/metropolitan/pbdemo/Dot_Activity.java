package com.metropolitan.pbdemo;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

public class Dot_Activity extends AppCompatActivity {

    private TextView tv_title, tv_description, tv_cat;
    private ImageView imageView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dot);


        tv_title = (TextView) findViewById(R.id.title);
        tv_description = (TextView) findViewById(R.id.description);
        tv_cat = (TextView) findViewById(R.id.cat_id);
        imageView = (ImageView) findViewById(R.id.img_id);

        //Recieve data
        Intent intent = getIntent();
        String Title = intent.getExtras().getString("Title");
        String Category = intent.getExtras().getString("Category");
        String Description = intent.getExtras().getString("Description");
        int image = intent.getExtras().getInt("Thumbnail");

        //Setting values

        tv_title.setText(Title);
        tv_description.setText(Description);
        tv_cat.setText(Category);
        imageView.setImageResource(image);
    }
}
