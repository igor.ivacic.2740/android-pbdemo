package com.metropolitan.pbdemo;

import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.GridView;

/**
 * Created by igor on 5/27/18.
 */

public class Gallery_Activity extends MainActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_gallery);

        GridView gridview = (GridView) findViewById(R.id.gridview);
        gridview.setAdapter(new ImageAdapter(this));
        gridview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent,
                                    View v, int position, long id){
                // Send intent to SingleViewActivity
                Intent i = new Intent(getApplicationContext(), SingleViewActivity.class);
                // Pass image index
                i.putExtra("id", position);
                startActivity(i);
            }
        });

    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case R.id.refresh_page:
                super.hideAndReconect();
                return true;

            case R.id.exit_app:
                super.onExit();
                return true;
            case R.id.make_a_call:
                super.makeACall();
                return true;
            case R.id.technologies:
                super.openTechnologies();
                return true;

            case R.id.zaposleni:
                super.teamMethod();
                return true;
            case R.id.see_gallery:
                showGallery();
                return true;

            case R.id.about_us:
                super.aboutUs();
                return true;
            case R.id.send_email:
                super.showEmail();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
