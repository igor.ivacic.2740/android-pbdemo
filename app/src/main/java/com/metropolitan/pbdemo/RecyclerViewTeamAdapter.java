package com.metropolitan.pbdemo;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

/**
 * Created by igor on 5/22/18.
 */

public class RecyclerViewTeamAdapter extends RecyclerView.Adapter<RecyclerViewTeamAdapter.MyViewHolder> {

    private Context mContext;
    private List<Team> mData;


    public RecyclerViewTeamAdapter(Context mContext, List<Team> mData) {
        this.mContext = mContext;
        this.mData = mData;
    }

    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view;

        LayoutInflater mInflater = LayoutInflater.from(mContext);
        view = mInflater.inflate(R.layout.card_item_dot, parent, false);


        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, final int position) {

        holder.tv_dot_title.setText(mData.get(position).getFirst_name());
        holder.img_dot_thumbnail.setImageResource(R.mipmap.pb);
        holder.cardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(mContext, TeamMemberActivity.class);
                // passing data to the dots activity
                intent.putExtra("FirstName", mData.get(position).getFirst_name());
                intent.putExtra("LastName", mData.get(position).getLast_name());
                intent.putExtra("Position", mData.get(position).getPosition());
                intent.putExtra("Thumbnail", R.mipmap.pb);
                //start the activity
                mContext.startActivity(intent);


            }
        });
        // Set click listener


    }

    @Override
    public int getItemCount() {
        return mData.size();
    }

    public static class MyViewHolder extends RecyclerView.ViewHolder {

        TextView tv_dot_title;
        ImageView img_dot_thumbnail;
        CardView cardView;

        public MyViewHolder(View itemView) {
            super(itemView);

            tv_dot_title = (TextView) itemView.findViewById(R.id.dot_title_id);
            img_dot_thumbnail = (ImageView) itemView.findViewById(R.id.dot_img_id);
            cardView = (CardView) itemView.findViewById(R.id.cardview_id);

        }

    }


}
