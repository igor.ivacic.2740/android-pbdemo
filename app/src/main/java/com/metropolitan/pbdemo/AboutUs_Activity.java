package com.metropolitan.pbdemo;

import android.app.Dialog;
import android.content.pm.PackageManager;
import android.location.Geocoder;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.Locale;

/**
 * Created by igor on 5/25/18.
 */

public class AboutUs_Activity extends MainActivity implements OnMapReadyCallback {
    private static final int TAG_CODE_PERMISSION_LOCATION = 1;

    private TextView _adresa_mapa;
    private TextView _email_mapa;
    GoogleMap mMap;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


        if (googleServicesAvailable()) {
            Toast.makeText(this, "PandaBirch na Mapi", Toast.LENGTH_LONG).show();
            setContentView(R.layout.activity_map);
            initMap();
        } else {

        }

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case R.id.refresh_page:
                super.hideAndReconect();
                return true;

            case R.id.exit_app:
                super.onExit();
                return true;
            case R.id.make_a_call:
                super.makeACall();
                return true;
            case R.id.technologies:
                super.openTechnologies();
                return true;

            case R.id.zaposleni:
                super.teamMethod();
                return true;
            case R.id.see_gallery:
                super.showGallery();
                return true;

            case R.id.about_us:
                aboutUs();
                return true;
            case R.id.send_email:
                super.showEmail();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }


    public boolean googleServicesAvailable() {
        GoogleApiAvailability api = GoogleApiAvailability.getInstance();
        int isAvailable = api.isGooglePlayServicesAvailable(this);
        if (isAvailable == ConnectionResult.SUCCESS) {
            return true;
        } else if (api.isUserResolvableError(isAvailable)) {
            Dialog dialog = api.getErrorDialog(this, isAvailable, 0);
            dialog.show();
        } else {
            Toast.makeText(this, "Cant connect to play services", Toast.LENGTH_LONG).show();
        }
        return false;
    }

    private void initMap() {
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);


    }


    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        //mMap.clear();
        //podešavam kontrolu zoom

        LatLng kg = new LatLng(44.804372, 20.458726);
        mMap.addMarker(new MarkerOptions().position(kg).title(kg.latitude + " " + kg.longitude));
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(kg, 17));
        //ovde ćemo kasnije podešavati tip mapa
        mMap.setMapType(GoogleMap.MAP_TYPE_SATELLITE);


        mMap.getUiSettings().setZoomControlsEnabled(true);
        //podešavam kontrolu kompas
        mMap.getUiSettings().setCompassEnabled(true);
        if (ContextCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) ==
                PackageManager.PERMISSION_GRANTED &&
                ContextCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION) ==
                        PackageManager.PERMISSION_GRANTED) {
            googleMap.setMyLocationEnabled(true);
            googleMap.getUiSettings().setMyLocationButtonEnabled(true);
        } else {
            //uključivanje dijaloga za potvrdu dozvole
            ActivityCompat.requestPermissions(this, new String[]{
                    android.Manifest.permission.ACCESS_FINE_LOCATION,
                    android.Manifest.permission.ACCESS_COARSE_LOCATION}, TAG_CODE_PERMISSION_LOCATION);
        }
        Geocoder gk = new Geocoder(getBaseContext(), Locale.getDefault());

    }

}
