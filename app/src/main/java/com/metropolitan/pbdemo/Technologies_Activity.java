package com.metropolitan.pbdemo;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.MenuItem;

import java.util.ArrayList;

/**
 * Created by igor on 5/27/18.
 */

public class Technologies_Activity extends MainActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_technologies);

        lstDot = new ArrayList<>();

        lstDot.add(new Dot("Data Science", "", "A data scientist is a professional responsible for collecting, analyzing and interpreting large amounts of data to identify ways to help a business improve operations and gain a competitive edge over rivals", R.drawable.data_science));
        lstDot.add(new Dot("Machine Learning", "", "Machine learning is an application of artificial intelligence (AI) that provides systems the ability to automatically learn and improve from experience without being explicitly programmed. Machine learning focuses on the development of computer programs that can access data and use it learn for themselves.", R.drawable.machine_learning));
        lstDot.add(new Dot("IOS", "", "OS (formerly iPhone OS) is a mobile operating system created and developed by Apple Inc. exclusively for its hardware. It is the operating system that presently powers many of the company's mobile devices, including the iPhone, iPad, and iPod Touch.", R.drawable.ios));
        lstDot.add(new Dot("Mobile Development", "", "Mobile app development is the act or process by which a mobile app is developed for mobile devices, such as personal digital assistants, enterprise digital assistants or mobile phones.", R.drawable.mobiledev));
        lstDot.add(new Dot("VR", "", "Virtual Reality (VR) is the use of computer technology to create a simulated environment. Unlike traditional user interfaces, VR places the user inside an experience. Instead of viewing a screen in front of them, users are immersed and able to interact with 3D worlds", R.drawable.vri));
        lstDot.add(new Dot("Web", "", "Web Developer Job Responsibilities: The role is responsible for designing, coding and modifying websites, from layout to function and according to a client's specifications. Strive to create visually appealing sites that feature user-friendly design and clear navigation.", R.drawable.webdev));
        lstDot.add(new Dot("Android", "", "An Android developer is responsible for developing applications for devices powered by the Android operating system. ... Writing a good job description or advertisement for an Android developer requires an emphasis to be placed on the specific technologies necessary for the project.", R.drawable.android));


        RecyclerView myrv = (RecyclerView) findViewById(R.id.recycleview_id);
        RecyclerViewAdapter myAdapter = new RecyclerViewAdapter(this, lstDot);
        myrv.setLayoutManager(new GridLayoutManager(this, 3));
        myrv.setAdapter(myAdapter);




    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case R.id.refresh_page:
                super.hideAndReconect();
                return true;

            case R.id.exit_app:
                super.onExit();
                return true;
            case R.id.make_a_call:
                super.makeACall();
                return true;
            case R.id.technologies:
                openTechnologies();
                return true;

            case R.id.zaposleni:
                super.teamMethod();
                return true;
            case R.id.see_gallery:
                super.showGallery();
                return true;

            case R.id.about_us:
                super.aboutUs();
                return true;
            case R.id.send_email:
                super.showEmail();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
