package com.metropolitan.pbdemo;


import android.database.Cursor;
import android.support.v7.app.AppCompatActivity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    private WebView webView;
    private ImageView imgLoading;
    private ImageView imgIgor;
    private TextView _adresa;
    private TextView _email;
    private TextView _tekuciRacun;
    private TextView _telefon;
    private LinearLayout _linearLayout;
    private boolean _status;
    private boolean _webViewWasActive;
    private ImageView imgPayment;
    private ProgressBar _progressBar;
    List lstDot;
    List lstTeam;
    Button btn;
    View v;


    private List<TextView> controls;

    public boolean getWasWebViewActive() {
        return _status;
    }

    public boolean setWasWebViewActive(boolean status) {
        this._status = status;
        return _status;
    }

    public void onClick(View v) {
        try {
            makeACall();
        } catch (Exception e) {
            Toast.makeText(this, e.getMessage(), Toast.LENGTH_LONG);
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        loadWebView();
        controls = new ArrayList<TextView>();


    }

    @Override
    public void onBackPressed() {
        if (isNetworkAvailable()) {
            if (webView.canGoBack()) {
                webView.goBack();
            } else {
                onExit();
            }
            return;
        } else {
            onExit();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case R.id.refresh_page:
                hideAndReconect();
                return true;

            case R.id.exit_app:
                onExit();
                return true;
            case R.id.make_a_call:
                makeACall();
                return true;
            case R.id.technologies:
                openTechnologies();
                return true;

            case R.id.zaposleni:
                teamMethod();
                return true;
            case R.id.see_gallery:
                showGallery();
                return true;

            case R.id.about_us:
                aboutUs();
                return true;
            case R.id.send_email:
                showEmail();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }


    public void teamMethod() {
        Intent newIntent = new Intent(this, Team_Activity.class);
        startActivity(newIntent);
//        try {
//            String destPath = "/data/data/" + getPackageName() +
//                    "/databases";
//            File f = new File(destPath);
//            if (!f.exists()) {
//                f.mkdirs();
//                f.createNewFile();
//                //---kopira db iz assets foldera u databases folder---
//                CopyDB(getBaseContext().getAssets().open("pb"),
//                        new FileOutputStream(destPath + "/pb"));
//
//            }
//        } catch (FileNotFoundException e) {
//            e.printStackTrace();
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
//
//        DBAdapter db = new DBAdapter(this);
//        db.open();
////        db.insertTeam(2,"Igor","Ivacic","Junior Developer");
//        db.insertTeam(23,"Srdjan","Stevanovic","Junior Developer");
//        db.insertTeam(22,"Petar","Ivacic","CTO");
//        db.insertTeam(12,"Pavle","Panovic","CEO");
//        db.insertTeam(11,"Igor","Kaplanovic","Data Science");
//
//        List<Team> lista = new ArrayList<Team>();
//
//        Cursor c = db.getAllTeam();
//        if (c.moveToFirst()) {
//            do {
//                lista.add(new Team(c.getInt(0), c.getString(1), c.getString(2), c.getString(3)));
//            } while (c.moveToNext());
//        }
//
//        db.close();
//        LinearLayout sc = (LinearLayout) findViewById(R.id.prostor);
//        TableLayout ll = new TableLayout(this);
//        TableLayout.LayoutParams lpt = new TableLayout.LayoutParams(TableLayout.LayoutParams.FILL_PARENT, TableLayout.LayoutParams.FILL_PARENT);
//        ll.setStretchAllColumns(true);
//        ll.setLayoutParams(lpt);
//        //TableLayout lt = (TableLayout) findViewById(R.id.table);
//        sc.addView(ll);
//        TableRow row1 = new TableRow(this);
//        TableRow.LayoutParams lp1 = new TableRow.LayoutParams(TableRow.LayoutParams.FILL_PARENT, TableRow.LayoutParams.FILL_PARENT);
//        row1.setLayoutParams(lp1);
//        TextView tv01 = new TextView(this);
//        TextView tv02 = new TextView(this);
//        TextView tv03 = new TextView(this);
//        TextView tv04 = new TextView(this);
//        tv01.setGravity(Gravity.START);
//        tv02.setGravity(Gravity.CENTER);
//        tv03.setGravity(Gravity.END);
//        tv01.setText("First_name");
//        tv02.setText("Last_name");
//        tv03.setText("Position");
//        row1.addView(tv01);
//        row1.addView(tv02);
//        row1.addView(tv03);
//        ll.addView(row1, new TableLayout.LayoutParams());
//        for (int i = 0; i < lista.size(); i++) {
//            TableRow row = new TableRow(this);
//            TableRow.LayoutParams lp = new TableRow.LayoutParams(TableRow.LayoutParams.WRAP_CONTENT, TableRow.LayoutParams.WRAP_CONTENT);
//            row.setLayoutParams(lp);
//            Team team = new Team();
//            team = lista.get(i);
//            TextView tv1 = new TextView(this);
//            TextView tv2 = new TextView(this);
//            TextView tv3 = new TextView(this);
////            TextView tv4 = new TextView(this);
//            tv1.setGravity(Gravity.START);
//            tv2.setGravity(Gravity.CENTER);
//            tv3.setGravity(Gravity.END);
//            tv1.setText(team.getFirst_name());
//            tv2.setText(team.getLast_name());
//            tv3.setText(team.getPosition());
//            row.addView(tv1);
//            row.addView(tv2);
//            row.addView(tv3);
//            ll.addView(row, new TableLayout.LayoutParams());
//        }

    }

    public void showEmail() {

        setContentView(R.layout.activity_email);
    }


    public void onClickEmail(View v) {
        final EditText emailtxt = (EditText) findViewById(R.id.editTextEmail);
        Intent i = new Intent(Intent.ACTION_SEND);
        i.setType("message/rfc822");
        i.putExtra(Intent.EXTRA_EMAIL, new String[]{"recipient@example.com"});
        i.putExtra(Intent.EXTRA_SUBJECT, "PandaBirch  PROMO");
        i.putExtra(Intent.EXTRA_TEXT, emailtxt.getText().toString());
        try {
            startActivity(Intent.createChooser(i, "Send mail..."));
        } catch (android.content.ActivityNotFoundException ex) {
            Toast.makeText(MainActivity.this, "There are no email clients installed.", Toast.LENGTH_SHORT).show();
        }
    }


    public void CopyDB(InputStream inputStream,
                       OutputStream outputStream) throws IOException {
        //---kopira 1KB u datom trenutku---
        byte[] buffer = new byte[1024];
        int length;
        while ((length = inputStream.read(buffer)) > 0) {
            outputStream.write(buffer, 0, length);
        }
        inputStream.close();
        outputStream.close();
    }


    public void aboutUs() {
        Intent newIntent = new Intent(this, AboutUs_Activity.class);
        startActivity(newIntent);


    }


    public void openTechnologies() {

        Intent newIntent = new Intent(this, Technologies_Activity.class);
        startActivity(newIntent);


    }


    protected void showGallery() {

        Intent newIntent = new Intent(this, Gallery_Activity.class);
        startActivity(newIntent);

    }


    protected void hideAndReconect() {
        hideAll(true);
//        loadWebView();
    }


    protected void makeACall() {
        try {
            Intent intent = new Intent(Intent.ACTION_DIAL);
            intent.setData(Uri.parse("tel:0112030885"));
            startActivity(intent);
            onExit();
        } catch (Exception e) {
            Toast.makeText(this, e.getMessage(), Toast.LENGTH_LONG);
        }
    }

    protected void onExit() {
        if (webView != null) {
            webView.clearCache(true);
        }
        System.exit(0);
    }


    private void loadWebView() {
        if (isNetworkAvailable()) {

            imgLoading = (ImageView) findViewById(R.id.logoOnNet);
            imgLoading.setVisibility(View.GONE);
            _progressBar = (ProgressBar) findViewById(R.id.progressBar);
            _progressBar.setVisibility(View.GONE);
            webView = (WebView) findViewById(R.id.webView1);
            webView.getSettings().setAppCacheEnabled(false);
            webView.getSettings().setCacheMode(WebSettings.LOAD_NO_CACHE);
            imgLoading.setVisibility(View.VISIBLE);
            _progressBar.setVisibility(View.VISIBLE);
            WebSettings webSettings = webView.getSettings();
            webSettings.setJavaScriptEnabled(true);
            webView.setWebViewClient(new WebViewClient() {
                public void onPageFinished(WebView view, String url) {
                    imgLoading.setVisibility(View.GONE);
                    _progressBar.setVisibility(View.GONE);
                    webView.setVisibility(View.VISIBLE);
                }
            });
            String url = (String) getText(R.string.url);
            webView.loadUrl(url);
            _webViewWasActive = true;
            setWasWebViewActive(true);

        } else {
            Toast.makeText(this, R.string.connectionMessageFailure, Toast.LENGTH_LONG).show();
            _adresa = (TextView) findViewById(R.id.address);
            _telefon = (TextView) findViewById(R.id.mobile_phone);
            _email = (TextView) findViewById(R.id.email);
            _tekuciRacun = (TextView) findViewById(R.id.tekuciRacun);
            _linearLayout = (LinearLayout) findViewById(R.id.linearLayout);

            _linearLayout.setBackgroundColor(getResources().getColor(R.color.MetropolitanColor));
            _adresa.setTextColor(Color.WHITE);
            _telefon.setTextColor(Color.WHITE);
            _email.setTextColor(Color.WHITE);
            _tekuciRacun.setTextColor(Color.WHITE);

            _adresa.setVisibility(View.VISIBLE);
            _telefon.setVisibility(View.VISIBLE);
            _email.setVisibility(View.VISIBLE);
            _tekuciRacun.setVisibility(View.VISIBLE);
            controls.add(_adresa);
            controls.add(_telefon);
            controls.add(_email);
            controls.add(_tekuciRacun);
            setWasWebViewActive(false);
        }
    }

    private void hideAll(boolean hidePayment) {
        try {
            if (hidePayment = true) {
                if (imgPayment != null) {
                    imgPayment.setVisibility(View.GONE);
                }
            }
            if ((getWasWebViewActive() == true) || (getWasWebViewActive() == false) || (getWasWebViewActive() == false && _webViewWasActive == true)) {
                if (imgLoading != null) {
                    imgLoading.setVisibility(View.GONE);
                }
                if (webView != null) {
                    webView.setVisibility(View.GONE);
                }
                if (_progressBar != null) {
                    _progressBar.setVisibility(View.GONE);
                }
                if (controls != null) {
                    hideTextViewControls(controls);
                }
            }
        } catch (Exception e) {
            Toast.makeText(this, e.getMessage(), Toast.LENGTH_LONG);
        }
    }

    private void hideTextViewControls(List<TextView> controls) {
        for (TextView textView : controls) {
            if (textView != null) {
                textView.setVisibility(View.GONE);
            }
        }
    }

    private boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }


}