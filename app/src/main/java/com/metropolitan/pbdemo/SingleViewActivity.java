package com.metropolitan.pbdemo;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.widget.ImageView;

/**
 * Created by igor on 5/27/18.
 */

public class SingleViewActivity extends Activity {
    int currentPosition = 0;
    int max;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.single_view);
        // Get intent data
        Intent i = getIntent();
        // Selected image id
        final int position = i.getExtras().getInt("id");
        final ImageAdapter imageAdapter = new ImageAdapter(this);
        currentPosition = position;
        max = imageAdapter.getCount();
        final ImageView imageView = (ImageView) findViewById(R.id.SingleView);
        imageView.setImageResource(imageAdapter.mThumbIds[position]);

        imageView.setOnTouchListener(new OnSwipeTouchListener(SingleViewActivity.this) {
            public void onSwipeRight() {
                imageView.setImageResource(imageAdapter.mThumbIds[currentPosDown()]);
            }

            public void onSwipeLeft() {
                imageView.setImageResource(imageAdapter.mThumbIds[currentPosUp()]);
            }

            public void onSwipeBottom() {
                returnToPreviousScreen();
            }

            public void onSwipeTop() {
                returnToPreviousScreen();
            }
        });
    }

    public void returnToPreviousScreen() {
        Intent i = new Intent(this, Gallery_Activity.class);
        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(i);
    }

    public int currentPosUp() {
        if (currentPosition == max - 1) {
            return currentPosition;
        } else {
            return currentPosition += 1;
        }
    }

    public int currentPosDown() {
        if (currentPosition == 0) {
            return currentPosition;
        } else {
            return currentPosition -= 1;
        }
    }
}