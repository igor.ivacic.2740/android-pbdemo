package com.metropolitan.pbdemo;


import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.List;


public class DBAdapter {

    static final String KEY_ID = "id";
    static final String KEY_FIRST_NAME = "first_name";
    static final String KEY_LAST_NAME = "last_name";
    static final String KEY_POSITION = "position";
    static final String TAG = "DBAdapter";

    static final String DATABASE_NAME = "pb";
    static final String DATABASE_TABLE = "team";
    static final int DATABASE_VERSION = 2;
    static final String DATABASE_CREATE =
            "create table team (id INTEGER primary key,first_name TEXT NOT NULL, last_name TEXT NOT NULL, position TEXT NOT NULL);";
    final Context context;
    DatabaseHelper DBHelper;
    SQLiteDatabase db;

    List<Team> list;

    public DBAdapter(Context ctx) {
        this.context = ctx;
        DBHelper = new DatabaseHelper(context);
    }

    private static class DatabaseHelper extends SQLiteOpenHelper {
        DatabaseHelper(Context context) {
            super(context, DATABASE_NAME, null, DATABASE_VERSION);
        }

        @Override
        public void onCreate(SQLiteDatabase db) {
            try {
                db.execSQL(DATABASE_CREATE);
            } catch (SQLException e) {
                e.printStackTrace();
            }

        }

        @Override
        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
            Log.w(TAG, "Ažuriranje verzije baze podataka sa " + oldVersion + " na verziju " + newVersion + ", a to će uništiti postojeće podatke");
            db.execSQL("DROP TABLE IF EXISTS team");
            onCreate(db);
        }
    }

    //---otvaranje baze podataka---
    public DBAdapter open() throws SQLException {
        db = DBHelper.getWritableDatabase();
        return this;
    }

    //---zatvaranje baze podataka---
    public void close() {
        DBHelper.close();
    }

    //---umetanje rezultata u bazu---
    public long insertTeam(int id, String first_name, String last_name, String position) {
        ContentValues initialValues = new ContentValues();
        initialValues.put(KEY_ID, id);
        initialValues.put(KEY_FIRST_NAME, first_name);
        initialValues.put(KEY_LAST_NAME, last_name);
        initialValues.put(KEY_POSITION, position);
        return db.insert(DATABASE_TABLE, null, initialValues);
    }

    //---brisanje konkretnog rezultata---
    public boolean deleteResult(long rowId) {
        return db.delete(DATABASE_TABLE, KEY_ID + "=" + rowId, null) > 0;
    }

    //---preuzima sve rezultate---
    public Cursor getAllTeam() {

//
//        Cursor  cursor = db.rawQuery("select * from team",null);
//
//
//        return cursor;

        return db.query(DATABASE_TABLE, new String[]{KEY_ID, KEY_FIRST_NAME,
                KEY_LAST_NAME, KEY_POSITION}, null, null, null, null, null);
    }

    //---preuzima konkretan rezultat---
    public Cursor getTeamById(long rowId) throws SQLException {
        Cursor mCursor =
                db.query(true, DATABASE_TABLE, new String[]{KEY_ID,
                                KEY_FIRST_NAME, KEY_LAST_NAME, KEY_POSITION}, KEY_ID + "=" + rowId, null,
                        null, null, null, null);
        if (mCursor != null) {
            mCursor.moveToFirst();
        }
        return mCursor;
    }

    //---ažurira rezultat---
    public boolean updateTeam(long rowId, String first_name, String last_name, String position) {
        ContentValues args = new ContentValues();
        args.put(KEY_FIRST_NAME, first_name);
        args.put(KEY_LAST_NAME, last_name);
        args.put(KEY_POSITION, position);
        return db.update(DATABASE_TABLE, args, KEY_ID + "=" + rowId, null) > 0;
    }

    public void CopyDB(InputStream inputStream,
                       OutputStream outputStream) throws IOException {
        //---kopira 1KB u datom trenutku---
        byte[] buffer = new byte[1024];
        int length;
        while ((length = inputStream.read(buffer)) > 0) {
            outputStream.write(buffer, 0, length);
        }
        inputStream.close();
        outputStream.close();
    }

}