package com.metropolitan.pbdemo;

import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;



/**
 * Created by igor on 5/25/18.
 */

public class Team_Activity extends MainActivity {

    private TextView tv_title, tv_description, tv_cat;
    private ImageView imageView;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_team);

        lstTeam = new ArrayList<>();
//        lstTeam.add(new Team(1,"Srdjan", "Stevanovic", "JD"));
//        lstTeam.add(new Team(2,"Igor", "Ivacic", "JD"));
//        lstTeam.add(new Team(3,"Petar", "Ivacic", "CTO"));
//        lstTeam.add(new Team(4,"Pavle", "Panovic", "CEO"));
//        lstTeam.add(new Team(5,"Ana", "Grujicic", "GD"));
//        lstTeam.add(new Team(6,"Dusan", "Svorcan", "I"));
        getTeam();
        RecyclerView myrv = (RecyclerView) findViewById(R.id.recycleview_id_team);
        RecyclerViewTeamAdapter myAdapter = new RecyclerViewTeamAdapter(this, lstTeam);
        myrv.setLayoutManager(new GridLayoutManager(this, 3));
        myrv.setAdapter(myAdapter);




    }

    public void getTeam(){
        DBAdapter db = new DBAdapter(this);
        db.open();

        Cursor c = db.getAllTeam();
        if (c.moveToFirst()) {
            do {
                lstTeam.add(new Team(c.getInt(0), c.getString(1), c.getString(2), c.getString(3)));
            } while (c.moveToNext());
        }

        db.close();

    }

    public void DispalayTeam(Cursor c) {
        Toast.makeText(this,
                "Indeks: " + c.getInt(0) + "\n" +
                        "Ime: " + c.getString(1) + "\n" +
                        "Prezime: " + c.getString(2) + "\n" +
                        "Pozicija: " + c.getString(3) + "\n",
                Toast.LENGTH_LONG).show();
    }


    public void getTeamById(int id) {
        //---preuzimanje jednog rezultata---
        DBAdapter db = new DBAdapter(this);
        db.open();
        Cursor d = db.getTeamById(id);
        if (d.moveToFirst())
            DispalayTeam(d);
        else
            Toast.makeText(this, "Nije pronađen ni jedan zaposlen",
                    Toast.LENGTH_LONG).show();
        db.close();
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case R.id.refresh_page:
                super.hideAndReconect();
                return true;

            case R.id.exit_app:
                super.onExit();
                return true;
            case R.id.make_a_call:
                super.makeACall();
                return true;
            case R.id.technologies:
                super.openTechnologies();
                return true;

            case R.id.zaposleni:
                teamMethod();
                return true;
            case R.id.see_gallery:
                super.showGallery();
                return true;

            case R.id.about_us:
                super.aboutUs();
                return true;
            case R.id.send_email:
                super.showEmail();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }


}
